#!/bin/bash

# ubuntu docker instalation

# First, update your existing list of packages:
apt -y update

# Next, install a few prerequisite packages which let apt use packages over HTTPS:
apt -y install apt-transport-https ca-certificates curl software-properties-common git wget vim ranger tmux glances

# Then add the GPG key for the official Docker repository to your system:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add the Docker repository to APT sources:
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

# Next, update the package database with the Docker packages from the newly added repo:
apt -y update

# Install docker on Linux
# Docker on Linux - install Edge release by script:
curl -sSL https://get.docker.com/ | sh
usermod -aG docker vagrant
# install docker-compose on Linux
# there is script: https://github.com/docker/compose/releases
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose
# install docker machine on Linux
# there is script: https://github.com/docker/machine/releases
curl -L https://github.com/docker/machine/releases/download/v0.16.1/docker-machine-`uname -s`-`uname -m` >/usr/local/bin/docker-machine && chmod +x /usr/local/bin/docker-machine

systemctl status docker

docker --version
docker-compose --version
docker-machine --version

